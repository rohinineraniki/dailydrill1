/*
NOTE: Do not change the name of this file

* Ensure that error handling is well tested.
* If there is an error at any point, the subsequent solutions must not get executed.
* Solutions without error handling will get rejected and you will be marked as having not completed this drill.

* Usage of async and await is not allowed.

Users API url: https://jsonplaceholder.typicode.com/users
Todos API url: https://jsonplaceholder.typicode.com/todos

Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=1
Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=1

Using promises and the `fetch` library, do the following. 

1. Fetch all the users
2. Fetch all the todos
3. Use the promise chain and fetch the users first and then the todos.
4. Use the promise chain and fetch the users first and then all the details for each user.
5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

NOTE: If you need to install `node-fetch` or a similar libary, let your mentor know before doing so along with the reason why. No other exteral libraries are allowed to be used.

Usage of the path libary is recommended


*/

// 1. Fetch all the users

let usersUrl = "https://jsonplaceholder.typicode.com/users";
let todosUrl = "https://jsonplaceholder.typicode.com/todos";

function fetchData(url) {
  return new Promise((resolve, reject) => {
    let options = {
      method: "GET",
    };
    fetch(url, options)
      .then((response) => {
        return response.json();
      })
      .then((userData) => {
        resolve(userData);
      })
      .catch((error) => {
        reject(error);
      });
  });
}
function fetchAllUsersData() {
  return new Promise((resolve, reject) => {
    fetchData(usersUrl)
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

fetchAllUsersData();

//2. Fetch all the todos
function fetchAllTodosData() {
  return new Promise((resolve, reject) => {
    fetchData(todosUrl)
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error);
      });
  });
}

fetchAllTodosData();
//3. Use the promise chain and fetch the users first and then the todos.

fetchData("https://jsonplaceholder.typicode.com/users")
  .then((data) => {
    console.log(data);
    return fetchData("https://jsonplaceholder.typicode.com/todos");
  })
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.log(error);
  });

// 4. Use the promise chain and fetch the users first and then all the details for each user.

function fetchUserDataById(id) {
  return new Promise((resolve, reject) => {
    let options = {
      method: "GET",
    };
    let userDataUrl = `https://jsonplaceholder.typicode.com/users?id=${id}`;
    fetch(userDataUrl, options)
      .then((data) => {
        resolve(data.json());
      })
      .catch((error) => {
        reject(error);
      });
  });
}

fetchData("https://jsonplaceholder.typicode.com/users")
  .then((data) => {
    console.log(data);
    const eachUserData = data.map((each) => {
      return fetchUserDataById(each.id);
    });
    return eachUserData;
  })
  .then((data) => {
    return Promise.all(data);
  })
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.log(error);
  });

// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo

function fetchFirstTodo() {
  return new Promise((resolve, reject) => {
    let url = "https://jsonplaceholder.typicode.com/todos?userId=1";
    fetchData(url)
      .then((data) => {
        let firstTodo = data.filter((each) => {
          if (each.id === 1) {
            return each;
          }
        });
        resolve(firstTodo);
      })
      .catch((error) => {
        console.log(error);
      });
  });
}
fetchFirstTodo()
  .then((data) => {
    console.log(data);
    let todoUserId = data[0].userId;
    let url = `https://jsonplaceholder.typicode.com/users?id=${todoUserId}`;
    return fetchData(url);
  })
  .then((data) => {
    console.log(data);
  })
  .catch((error) => {
    console.log(error);
  });
