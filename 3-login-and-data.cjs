/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)


Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/

const fs = require("fs");
const path = require("path");

function creatingFile1() {
  let path1 = path.join(__dirname, "file1.json");
  return new Promise((resolve, reject) => {
    fs.writeFile(path1, "", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}
creatingFile1()
  .then((data) => {
    console.log("Created File1 successfully");
  })
  .catch((error) => {
    console.log(error);
  });

function creatingFile2() {
  let path2 = path.join(__dirname, "file2.json");
  return new Promise((resolve, reject) => {
    fs.writeFile(path2, "", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

creatingFile2()
  .then((data) => {
    console.log("Created File2 successfully");
  })
  .catch((error) => {
    console.log(error);
  });

function deleteFiles(path) {
  return new Promise((resolve, reject) => {
    fs.unlink(path, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function deleteFile1() {
  let path1 = path.join(__dirname, "file1.json");
  let path2 = path.join(__dirname, "file2.json");
  return new Promise((resolve, reject) => {
    let fileNames = [path1, path2];
    setTimeout(() => {
      let deleteing2Files = fileNames.map((eachFile) => {
        return deleteFiles(eachFile);
      });
      Promise.all(deleteing2Files)
        .then((data) => {
          console.log("Deleted 2 files successfully");
        })
        .catch((error) => {
          console.log(error);
        });
    }, 2000);
  });
}
deleteFile1()
  .then((data) => {
    console.log("Deleted file1 successfully");
  })
  .catch((error) => {
    console.log(error);
  });

// Q2. Create a new file with lipsum data (you can google and get this).
// Do File Read and write data to another file
// Delete the original file
// Using promise chaining

function createLipsumData() {
  let data =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam quis elit nisl. Aliquam congue ut orci quis gravida. Etiam eleifend ultrices turpis, ut gravida ligula accumsan vel. Cras sagittis ex ac rutrum congue. Curabitur tristique nibh posuere nisi varius, non ullamcorper sapien venenatis. Maecenas non suscipit ex. Etiam commodo magna ullamcorper ex dignissim, sit amet lacinia dolor molestie. Morbi vitae scelerisque magna. Phasellus pulvinar, mauris id ornare consequat, elit sem fermentum nisl, eget varius sapien risus at felis.Curabitur eu accumsan turpis. Fusce finibus sem euismod massa fringilla, at maximus sem pulvinar. Proin sollicitudin porta leo sed pellentesque. Mauris nisi dolor, posuere in neque sit amet, sodales lobortis justo. Etiam venenatis metus aliquet purus porta, a aliquam nunc ullamcorper. Maecenas dapibus nibh non nunc facilisis, sit amet consequat lectus scelerisque. Pellentesque neque elit, sodales a commodo vel, vulputate in diam. Quisque quis lacus hendrerit, viverra dolor vel, volutpat nisi. Sed facilisis sollicitudin lorem, sit amet vulputate ipsum convallis sit amet. Praesent ipsum orci, vestibulum non consectetur id, molestie sit amet sem. Phasellus interdum purus id erat tempor rhoncus. In viverra quam et neque placerat malesuada. Donec interdum nulla non ante finibus, eget posuere sem ultricies. Vestibulum tempor aliquam ipsum sit amet pretium. Quisque egestas risus nisi, vel pharetra arcu ultricies a. Quisque ornare nunc sit amet libero pharetra consectetur. Aenean ligula dui, faucibus vitae tempor in, molestie ut nibh. Donec finibus ex ex, et aliquet urna accumsan et. Duis felis nisi, tincidunt nec magna sed, tincidunt volutpat lectus. Mauris at gravida ipsum. Duis vel velit lectus. Etiam auctor fermentum tortor, eget condimentum elit. ";
  let dataPath = path.join(__dirname, "lipsum.txt");
  return new Promise((resolve, reject) => {
    fs.writeFile(dataPath, data, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function readLipsumData() {
  let dataPath = path.join(__dirname, "lipsum.txt");
  return new Promise((resolve, reject) => {
    fs.readFile(dataPath, "utf-8", (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

function writeLipsumData(data) {
  let writePath = path.join(__dirname, "lipsumData.txt");
  return new Promise((resolve, reject) => {
    fs.writeFile(writePath, data, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

function deleteOriginalFile() {
  let dataPath = path.join(__dirname, "lipsum.txt");
  return new Promise((resolve, reject) => {
    fs.unlink(dataPath, (err, data) => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}
createLipsumData()
  .then((data) => {
    console.log("Got lipsum Data successfully");
    return readLipsumData();
  })
  .then((data) => {
    console.log(data);
    return writeLipsumData(data);
  })
  .then((data) => {
    console.log("Written into new file");
    return deleteOriginalFile();
  })
  .then((data) => {
    console.log("Original lipsum.txt file deleted");
  })
  .catch((error) => {
    console.log(error);
  });

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/

function login(user, val) {
  if (val % 2 === 0) {
    return Promise.resolve(user);
  } else {
    return Promise.reject(new Error("User not found"));
  }
}

function getData() {
  return Promise.resolve([
    {
      id: 1,
      name: "Test",
    },
    {
      id: 2,
      name: "Test 2",
    },
  ]);
}

function logData(user, activity) {
  // use promises and fs to save activity in some file
  return new Promise((resolve, reject) => {
    user["activity"] = activity;
    user["createdAt"] = new Date();
    console.log(user);
    let filePath = path.join(__dirname, "log-data.log");
    fs.appendFile(filePath, JSON.stringify(user), (error, data) => {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    });
  });
}

function createUser(user, val) {
  login(user, val)
    .then((data) => {
      return logData(data, "Login Success");
    })
    .then((data) => {
      return getData(data);
    })
    .then((data) => {
      return logData(data, "GetData Success");
    })
    .catch((error) => {
      if (error.message === "User not found") {
        console.log(error);
        logData(user, "Login Failure")
          .then((data) => {
            return logData(user, "GetData Failure");
          })
          .then((data) => {
            console.log("Writing failure activity in logdata is success");
          })
          .catch((error) => {
            console.log("Writing failure activity in logdata is failed");
          });
      } else {
        console.log("Writing log data fails");
        console.log(error);
      }
    });
}
let user = {
  id: 3,
  name: "Rohini",
};
let val = 3;

createUser(user, val);
