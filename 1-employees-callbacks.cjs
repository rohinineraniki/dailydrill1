/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file



const data = {
    "employees": [
        {
            "id": 23,
            "name": "Daphny",
            "company": "Scooby Doo"
        },
        {
            "id": 73,
            "name": "Buttercup",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 93,
            "name": "Blossom",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 13,
            "name": "Fred",
            "company": "Scooby Doo"
        },
        {
            "id": 89,
            "name": "Welma",
            "company": "Scooby Doo"
        },
        {
            "id": 92,
            "name": "Charles Xavier",
            "company": "X-Men"
        },
        {
            "id": 94,
            "name": "Bubbles",
            "company": "Powerpuff Brigade"
        },
        {
            "id": 2,
            "name": "Xyclops",
            "company": "X-Men"
        }
    ]
}

*/

const fs = require("fs");
const path = require("path");
const { format } = require("date-fns");
let data_path = path.join(__dirname, "data.json");
fs.readFile(data_path, "utf-8", function (error, data) {
  if (error) {
    console.log(error);
  } else {
    let parsed_data = JSON.parse(data);
    let retrive_ids = [2, 13, 23];
    let get_key = Object.keys(parsed_data);
    let filtered_array = [];
    parsed_data[get_key].filter((each) => {
      if (retrive_ids.includes(each.id)) {
        filtered_array.push(each);
      }
    });
    let result = {};
    result[get_key[0]] = filtered_array;
    let stringify_data_1 = JSON.stringify(result);
    let result_path_1 = path.join(__dirname, "retrive-data-ids.json");
    fs.writeFile(result_path_1, stringify_data_1, function (error, data) {
      if (error) {
        console.error(error);
      } else {
        console.log("Written output of problem1 successfully");
        let group_by_company = parsed_data[get_key].reduce((output, each) => {
          output[each["company"]] ?? (output[each["company"]] = []);
          output[each["company"]].push(each);
          return output;
        }, {});
        let stringify_data_2 = JSON.stringify(group_by_company);
        let result_path_2 = path.join(__dirname, "group-by-company.json");
        fs.writeFile(result_path_2, stringify_data_2, function (error, data) {
          if (error) {
            console.error(error);
          } else {
            console.log("Written output of problem2 successfully");
            let company_names = Object.keys(group_by_company);
            let filter_by_company = company_names.reduce((output, each) => {
              if (each === "Powerpuff Brigade") {
                output["Powerpuff Brigade"] = group_by_company[each];
                return output;
              }
              return output;
            }, {});
            let stringify_data_3 = JSON.stringify(filter_by_company);
            let result_path_3 = path.join(__dirname, "filter-by-company.json");
            fs.writeFile(
              result_path_3,
              stringify_data_3,
              function (error, data) {
                if (error) {
                  console.log(error);
                } else {
                  console.log("Written output of problem3 successfully");
                  let copy_parsed_data = { ...parsed_data };
                  let new_key = Object.keys(copy_parsed_data);
                  let parsed_array = copy_parsed_data[new_key];
                  const remove_id_2 = parsed_array.filter((each, index) => {
                    if (Number(each.id) === 2) {
                      copy_parsed_data[get_key].splice(index, 1);
                    } else {
                      return each;
                    }
                  });
                  let new_object = {};
                  new_object["employees"] = remove_id_2;
                  let stringify_data_4 = JSON.stringify(new_object);
                  let result_path_4 = path.join(
                    __dirname,
                    "data-withpout-id-2.json"
                  );
                  fs.writeFile(
                    result_path_4,
                    stringify_data_4,
                    function (error, data) {
                      if (error) {
                        console.log(error);
                      } else {
                        console.log("Written output of problem4 successfully");
                        function compareFn(item_1, item_2) {
                          let item_1_company = item_1.company;
                          let item_2_company = item_2.company;
                          let item_1_id = item_1.id;
                          let item_2_id = item_2.id;
                          if (item_1_company > item_2_company) {
                            return 1;
                          }
                          if (item_1_company < item_2_company) {
                            return -1;
                          } else {
                            return item_1_id - item_2_id;
                          }
                        }
                        let data_tobe_sorted = { ...parsed_data };
                        let new_key = Object.keys(data_tobe_sorted);
                        let sorted_data =
                          data_tobe_sorted[new_key].sort(compareFn);
                        let sorted_object = {};
                        sorted_object["employees"] = sorted_data;
                        let stringify_data_5 = JSON.stringify(sorted_object);
                        let result_path_5 = path.join(
                          __dirname,
                          "sorted-data.json"
                        );
                        fs.writeFile(
                          result_path_5,
                          stringify_data_5,
                          function (error, data) {
                            if (error) {
                              console.error(error);
                            } else {
                              console.log(
                                "Written output of problem5 successfully"
                              );
                              let copy_parsed_data = { ...parsed_data };

                              let parsed_array = copy_parsed_data[get_key];
                              let get_index = [];
                              let swap_elements = parsed_array.reduce(
                                (output, each, index) => {
                                  if (each.id === 93 || each.id === 92) {
                                    get_index.push(index);
                                    parsed_array.splice(index, 1);
                                    output.push(each);
                                  }
                                  return output;
                                },

                                []
                              );
                              swap_elements[0].company =
                                swap_elements[1].company;
                              swap_elements[1].company =
                                swap_elements[0].company;

                              parsed_array.splice(
                                get_index[0],
                                0,
                                swap_elements[0]
                              );
                              parsed_array.splice(
                                get_index[1],
                                0,
                                swap_elements[1]
                              );

                              let swaped_object = {};
                              swaped_object["employees"] = parsed_array;
                              let stringify_data_6 =
                                JSON.stringify(swaped_object);
                              let result_path_6 = path.join(
                                __dirname,
                                "swaped-data.json"
                              );
                              fs.writeFile(
                                result_path_6,
                                stringify_data_6,
                                (error, data) => {
                                  if (error) {
                                    console.error(error);
                                  } else {
                                    console.log(
                                      "Written output of problem6 successfully"
                                    );
                                    let add_field_dateofbirth = parsed_data[
                                      get_key
                                    ].map((each) => {
                                      each["date_of_birth"] = format(
                                        new Date(),
                                        "dd-MM-yyyy"
                                      );

                                      return each;
                                    });
                                    let add_field_object = {};
                                    add_field_object["employees"] =
                                      add_field_dateofbirth;
                                    let stringify_data_7 =
                                      JSON.stringify(add_field_object);
                                    let result_path_7 = path.join(
                                      __dirname,
                                      "add-filed-dateofbirth.json"
                                    );
                                    fs.writeFile(
                                      result_path_7,
                                      stringify_data_7,
                                      function (error, data) {
                                        if (error) {
                                          console.log(error);
                                        } else {
                                          console.log(
                                            "Written output of problem7 successfully"
                                          );
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    }
                  );
                }
              }
            );
          }
        });
      }
    });
  }
});
