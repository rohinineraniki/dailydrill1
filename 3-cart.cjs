const products = [
  {
    shampoo: {
      price: "$50",
      quantity: 4,
    },
    "Hair-oil": {
      price: "$40",
      quantity: 2,
      sealed: true,
    },
    comb: {
      price: "$12",
      quantity: 1,
    },
    utensils: [
      {
        spoons: { quantity: 2, price: "$8" },
      },
      {
        glasses: { quantity: 1, price: "$70", type: "fragile" },
      },
      {
        cooker: { quantity: 4, price: "$900" },
      },
    ],
    watch: {
      price: "$800",
      quantity: 1,
      type: "fragile",
    },
  },
];

/*

Q1. Find all the items with price more than $65.
Q2. Find all the items where quantity ordered is more than 1.
Q.3 Get all items which are mentioned as fragile.
Q.4 Find the least and the most expensive item for a single quantity.
Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

NOTE: Do not change the name of this file

*/
//Q1. Find all the items with price more than $65.
function priceMorethan65(products) {
  let productKeys = Object.keys(products[0]);
  let priceMorethan65Result = productKeys.reduce((output, each) => {
    if (Array.isArray(products[0][each])) {
      const filteredPrice = products[0][each].filter((eachValue, index) => {
        let valueKey = Object.keys(products[0][each][index]);
        let priceValue = Number(
          products[0][each][index][valueKey].price.slice(1)
        );
        if (priceValue > 65) {
          return true;
        }
      });
      output[each] = filteredPrice;
      return output;
    } else {
      let priceValue = Number(products[0][each].price.slice(1));

      if (priceValue > 65) {
        output[each] = products[0][each];
        return output;
      }
    }
    return output;
  }, {});
  return priceMorethan65Result;
}
const priceMorethan65Array = priceMorethan65(products);
console.log(priceMorethan65Array);

//Q2. Find all the items where quantity ordered is more than 1.

function quantityMorethan1(products) {
  let productKeys = Object.keys(products[0]);
  let quantityMorethan1Result = productKeys.reduce((output, each) => {
    if (Array.isArray(products[0][each])) {
      const filteredQuantity = products[0][each].filter((eachValue, index) => {
        let valueKey = Object.keys(products[0][each][index]);
        let quantityValue = Number(products[0][each][index][valueKey].quantity);
        if (quantityValue > 1) {
          return true;
        }
      });
      output[each] = filteredQuantity;
      return output;
    } else {
      let quantityValue = Number(products[0][each].quantity);
      if (quantityValue > 1) {
        output[each] = products[0][each];
        return output;
      }
    }
    return output;
  }, {});
  return quantityMorethan1Result;
}
const quantityMorethan1Array = quantityMorethan1(products);
console.log(quantityMorethan1Array);

//Q.3 Get all items which are mentioned as fragile.

function fragileItem(products) {
  let productKeys = Object.keys(products[0]);
  let fragileItemResult = productKeys.reduce((output, each) => {
    if (Array.isArray(products[0][each])) {
      const filteredQuantity = products[0][each].filter((eachValue, index) => {
        let valueKey = Object.keys(products[0][each][index]);
        let fragileType = products[0][each][index][valueKey].type;
        if (fragileType === "fragile") {
          return true;
        }
      });
      output[each] = filteredQuantity;
      return output;
    } else {
      let fragileType = products[0][each].type;
      if (fragileType === "fragile") {
        output[each] = products[0][each];
        return output;
      }
    }
    return output;
  }, {});
  return fragileItemResult;
}
const fragileItemArray = fragileItem(products);
console.log(fragileItemArray);

// Q.4 Find the least and the most expensive item for a single quantity.

function leastAndExpensiveItem(products) {
  let productKeys = Object.keys(products[0]);
  let quantityEqualTo1Result = productKeys.reduce((output, each) => {
    if (Array.isArray(products[0][each])) {
      const filteredQuantity = products[0][each].filter((eachValue, index) => {
        let valueKey = Object.keys(products[0][each][index]);
        let quantityValue = Number(products[0][each][index][valueKey].quantity);
        if (quantityValue === 1) {
          return true;
        }
      });
      output[each] = filteredQuantity;
      return output;
    } else {
      let quantityValue = Number(products[0][each].quantity);
      if (quantityValue === 1) {
        output[each] = products[0][each];
        return output;
      }
    }
    return output;
  }, {});
  console.log(quantityEqualTo1Result);
  function compareFn(item1, item2) {
    let item1Price;
    let item2Price;
    if (Array.isArray(quantityEqualTo1Result[item1])) {
      item1Price = Object.values(
        quantityEqualTo1Result[item1][0]
      )[0].price.slice(1);
    } else {
      item1Price = quantityEqualTo1Result[item1].price.slice(1);
    }
    if (Array.isArray(quantityEqualTo1Result[item2])) {
      item2Price = Object.values(
        quantityEqualTo1Result[item2][0]
      )[0].price.slice(1);
    } else {
      const priceMorethan65Array = priceMorethan65(products);
      // console.log(priceMorethan65Array);
      item2Price = quantityEqualTo1Result[item2].price.slice(1);
    }
    if (Number(item1Price) > Number(item2Price)) {
      return 1;
    }
    if (Number(item1Price) < Number(item2Price)) {
      return -1;
    } else {
      return 0;
    }
    //console.log(item1Price);
  }
  let arrayKeys = Object.keys(quantityEqualTo1Result);
  const sortResult = arrayKeys.sort(compareFn);
  let leastAndExpensive = sortResult.reduce((output, each, index) => {
    if (index === 0 || index === sortResult.length - 1) {
      output[each] = quantityEqualTo1Result[each];
      return output;
    }
    return output;
  }, {});
  let leastCost = quantityEqualTo1Result[sortResult[0]];
  // console.log(leastCost);
  return leastAndExpensive;
}

const leastAndExpensiveItemArray = leastAndExpensiveItem(products);
console.log(leastAndExpensiveItemArray);

// Q.5 Group items based on their state of matter at room temperature (Solid, Liquid Gas)

function groupByMatterState(products) {
  let productKeys = Object.keys(products[0]);
  let groupByMatterStateArray = productKeys.reduce((output, each) => {
    if (each === "shampoo" || each === "Hair-oil") {
      output["Liquid"] ?? (output["Liquid"] = []);
      let newObj = {};
      newObj[each] = products[0][each];
      output["Liquid"].push(newObj);
      return output;
    }
    if (each === "comb" || each === "utensils" || each === "watch") {
      output["Solid"] ?? (output["Solid"] = []);
      let newObj = {};
      newObj[each] = products[0][each];
      output["Solid"].push(newObj);
      return output;
    }
    return output;
  }, {});
  return groupByMatterStateArray;
}
const groupByMatterStateArray = groupByMatterState(products);
console.log(groupByMatterStateArray);
