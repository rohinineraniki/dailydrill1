const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interests: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/

//Q1 Find all users who are interested in playing video games.

function video_game_users(users) {
  let users_array = Object.keys(users);

  const video_game_users_array = users_array.reduce((output, each) => {
    if (users[each].interests[0].includes("Video Games")) {
      output[each] = users[each];
      return output;
    }
    return output;
  }, {});
  return video_game_users_array;
}
const video_game_users_array = video_game_users(users);
console.log(video_game_users_array);

//Q2 Find all users staying in Germany.

function stay_in_germany(users) {
  let users_array = Object.keys(users);
  const stay_in_germany_array = users_array.reduce((output, each) => {
    if (users[each].nationality === "Germany") {
      output[each] = users[each];
      return output;
    }
    return output;
  }, {});
  return stay_in_germany_array;
}

const stay_in_germany_array = stay_in_germany(users);
console.log(stay_in_germany_array);

// Q3 Sort users based on their seniority level
//    for Designation - Senior Developer > Developer > Intern
//    for Age - 20 > 10
function sort_by_seniority_and_age(users) {
  let users_array = Object.keys(users);
}
const sort_by_seniority_and_age_array = sort_by_seniority_and_age(users);
console.log(sort_by_seniority_and_age_array);

// Q4 Find all users with masters Degree.

function users_with_masters(users) {
  let users_array = Object.keys(users);
  const users_with_masters_array = users_array.reduce((output, each) => {
    if (users[each].qualification === "Masters") {
      output[each] = users[each];
      return output;
    }
    return output;
  }, {});
  return users_with_masters_array;
}

const users_with_masters_array = users_with_masters(users);
console.log(users_with_masters_array);

// Q5 Group users based on their Programming language mentioned in their designation.

function group_by_programming_language(users) {
  let users_array = Object.keys(users);
  let language_array = ["Golang", "Python", "Javascript"];
  const group_by_array = users_array.reduce((output, each) => {
    if (users[each].desgination.includes(language_array[0])) {
      console.log(users[each].desgination);
      output[language_array[0]] ?? (output[language_array[0]] = []);
      output[language_array[0]].push(each);
    } else if (users[each].desgination.includes(language_array[1])) {
      console.log(users[each].desgination);
      output[language_array[1]] ?? (output[language_array[1]] = []);
      output[language_array[1]].push(each);
    } else if (users[each].desgination.includes(language_array[2])) {
      console.log(users[each].desgination);
      output[language_array[2]] ?? (output[language_array[2]] = []);
      output[language_array[2]].push(each);
    }

    return output;
  }, {});
  return group_by_array;
}

const group_by_programming_language_array =
  group_by_programming_language(users);
console.log(group_by_programming_language_array);
