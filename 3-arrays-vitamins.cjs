const items = [
  {
    name: "Orange",
    available: true,
    contains: "Vitamin C",
  },
  {
    name: "Mango",
    available: true,
    contains: "Vitamin K, Vitamin C",
  },
  {
    name: "Pineapple",
    available: true,
    contains: "Vitamin A",
  },
  {
    name: "Raspberry",
    available: false,
    contains: "Vitamin B, Vitamin A",
  },
  {
    name: "Grapes",
    contains: "Vitamin D",
    available: false,
  },
];

/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/

//1. Get all items that are available

function avalible_items(items) {
  let items_array = items.filter((each) => {
    if (each.available) {
      return each;
    }
  });
  return items_array;
}
const avalible_items_result = avalible_items(items);
console.log(avalible_items_result);

//2. Get all items containing only Vitamin C.

function vitamin_c_items(items) {
  let items_array = items.filter((each) => {
    if (each.contains.includes("Vitamin C")) {
      return each;
    }
  });
  return items_array;
}
const vitamin_c_items_result = vitamin_c_items(items);
console.log(vitamin_c_items_result);

//3. Get all items containing Vitamin A.

function vitamin_a_items(items) {
  let items_array = items.filter((each) => {
    if (each.contains.includes("Vitamin A")) {
      return each;
    }
  });
  return items_array;
}
const vitamin_a_items_result = vitamin_a_items(items);
console.log(vitamin_a_items_result);

/*
4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
*/

function group_by_vitamins(items) {
  const group_by_vitamins_array = items.reduce((output, each) => {
    let vitamin_name_array = each.contains.split(", ");
    const array_output = vitamin_name_array.map((vitamin) => {
      output[vitamin] ?? (output[vitamin] = []);
      if (each.contains.includes(vitamin)) {
        return output[vitamin].push(each.name);
      }
    });

    return output;
  }, {});
  return group_by_vitamins_array;
}
const group_by_vitamins_array = group_by_vitamins(items);
console.log(group_by_vitamins_array);

//5. Sort items based on number of Vitamins they contain.

function sort_by_vitamin_number(items) {
  const items_array = items.sort((name1, name2) => {
    if (name1.contains.split(",").length > name2.contains.split(",").length) {
      return 1;
    }
    if (name1.contains.split(",").length < name2.contains.split(",").length) {
      return -1;
    }
    return 0;
  });
  return items_array;
}
const sorted_array = sort_by_vitamin_number(items);
console.log(sorted_array);
