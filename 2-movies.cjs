const favouritesMovies = {
  Matrix: {
    imdbRating: 8.3,
    actors: ["Keanu Reeves", "Carrie-Anniee"],
    oscarNominations: 2,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$680M",
  },
  FightClub: {
    imdbRating: 8.8,
    actors: ["Edward Norton", "Brad Pitt"],
    oscarNominations: 6,
    genre: ["thriller", "drama"],
    totalEarnings: "$350M",
  },
  Inception: {
    imdbRating: 8.3,
    actors: ["Tom Hardy", "Leonardo Dicaprio"],
    oscarNominations: 12,
    genre: ["sci-fi", "adventure"],
    totalEarnings: "$870M",
  },
  "The Dark Knight": {
    imdbRating: 8.9,
    actors: ["Christian Bale", "Heath Ledger"],
    oscarNominations: 12,
    genre: ["thriller"],
    totalEarnings: "$744M",
  },
  "Pulp Fiction": {
    imdbRating: 8.3,
    actors: ["Sameul L. Jackson", "Bruce Willis"],
    oscarNominations: 7,
    genre: ["drama", "crime"],
    totalEarnings: "$455M",
  },
  Titanic: {
    imdbRating: 8.3,
    actors: ["Leonardo Dicaprio", "Kate Winslet"],
    oscarNominations: 13,
    genre: ["drama"],
    totalEarnings: "$800M",
  },
};

/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/
//Q1. Find all the movies with total earnings more than $500M.

function earningsMorethan500(favouritesMovies) {
  let moviesArray = Object.keys(favouritesMovies);
  const earningsMorethan500Array = moviesArray.reduce((acc, each) => {
    let earnings = favouritesMovies[each].totalEarnings;
    let earningValue = Number(earnings.slice(1, earnings.length - 1));
    if (earningValue > 500) {
      acc[each] = favouritesMovies[each];
      return acc;
    }
    return acc;
  }, {});
  return earningsMorethan500Array;
}
const earningsMorethan500Array = earningsMorethan500(favouritesMovies);
console.log(earningsMorethan500Array);

//Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.

function oscarMorethan3AndEarningsMorethan500(favouritesMovies) {
  let moviesArray = Object.keys(favouritesMovies);
  const result = moviesArray.reduce((acc, each) => {
    let earnings = favouritesMovies[each].totalEarnings;
    let earningValue = Number(earnings.slice(1, earnings.length - 1));
    let oscarCount = favouritesMovies[each].oscarNominations;
    if (earningValue > 500 && oscarCount > 3) {
      acc[each] = favouritesMovies[each];
      return acc;
    }
    return acc;
  }, {});
  return result;
}
const oscarMorethan3AndEarningsMorethan500Result =
  oscarMorethan3AndEarningsMorethan500(favouritesMovies);
console.log(oscarMorethan3AndEarningsMorethan500Result);

//Q.3 Find all movies of the actor "Leonardo Dicaprio".
function leonardoMovies(favouritesMovies) {
  let moviesArray = Object.keys(favouritesMovies);
  const leonardoMoviesResult = moviesArray.reduce((acc, each) => {
    if (favouritesMovies[each].actors.includes("Leonardo Dicaprio")) {
      acc[each] = favouritesMovies[each];
      return acc;
    }
    return acc;
  }, {});
  return leonardoMoviesResult;
}
const leonardoMoviesResult = leonardoMovies(favouritesMovies);
console.log(leonardoMoviesResult);

//  Q.4 Sort movies (based on IMDB rating)
//if IMDB ratings are same, compare totalEarning as the secondary metric.

function sortByImdb(favouritesMovies) {
  let moviesArray = Object.keys(favouritesMovies);
  function compareFn(movie1, movie2) {
    let movie1Rating = favouritesMovies[movie1].imdbRating;
    let movie2Rating = favouritesMovies[movie2].imdbRating;
    let earnings1 = favouritesMovies[movie1].totalEarnings;
    let earningValue1 = Number(earnings1.slice(1, earnings1.length - 1));
    let earnings2 = favouritesMovies[movie2].totalEarnings;
    let earningValue2 = Number(earnings2.slice(1, earnings2.length - 1));

    if (movie1Rating > movie2Rating) {
      return 1;
    }
    if (movie1Rating < movie2Rating) {
      return -1;
    } else {
      if (earningValue1 > earningValue2) {
        return 1;
      }
      if (earningValue1 < earningValue2) {
        return -1;
      } else {
        return 0;
      }
    }
  }

  const sortedArray = moviesArray.sort(compareFn);
  const sortedObject = sortedArray.reduce((acc, each) => {
    acc[each] = favouritesMovies[each];
    return acc;
  }, {});
  return sortedObject;
}
const sortByImdbResult = sortByImdb(favouritesMovies);
console.log(sortByImdbResult);

//Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
//drama > sci-fi > adventure > thriller > crime

function groupByGenre(favouritesMovies) {
  let moviesArray = Object.keys(favouritesMovies);
  let ouputObj = {};

  let groupByGenreResult = moviesArray.reduce((acc, each) => {
    acc["drama"] ?? (acc["drama"] = []);
    acc["sci-fi"] ?? (acc["sci-fi"] = []);
    acc["adventure"] ?? (acc["adventure"] = []);
    acc["thriller"] ?? (acc["thriller"] = []);
    acc["crime"] ?? (acc["crime"] = []);
    if (favouritesMovies[each].genre.includes("drama")) {
      let newObj = {};
      newObj[each] = favouritesMovies[each];
      acc["drama"].push(newObj);
      return acc;
    } else if (favouritesMovies[each].genre.includes("sci-fi")) {
      if (!acc[each]) {
        let newObj = {};
        newObj[each] = favouritesMovies[each];
        acc["sci-fi"].push(newObj);
        return acc;
      }
      return acc;
    } else if (favouritesMovies[each].genre.includes("adventure")) {
      if (!acc[each]) {
        let newObj = {};
        newObj[each] = favouritesMovies[each];
        acc["adventure"].push(newObj);
        return acc;
      }
      return acc;
    } else if (favouritesMovies[each].genre.includes("thriller")) {
      if (!acc[each]) {
        let newObj = {};
        newObj[each] = favouritesMovies[each];
        acc["thriller"].push(newObj);
        return acc;
      }
    } else if (favouritesMovies[each].genre.includes("crime")) {
      if (!acc[each]) {
        let newObj = {};
        newObj[each] = favouritesMovies[each];
        acc["crime"].push(newObj);
        return acc;
      }
      return acc;
    }

    return acc;
  }, {});

  return groupByGenreResult;
}
const groupByGenreResult = groupByGenre(favouritesMovies);
console.log(groupByGenreResult);
