let cards_array = [
  {
    id: 1,
    card_number: "5602221055053843723",
    card_type: "china-unionpay",
    issue_date: "5/25/2021",
    salt: "x6ZHoS0t9vIU",
    phone: "339-555-5239",
  },
  {
    id: 2,
    card_number: "3547469136425635",
    card_type: "jcb",
    issue_date: "12/18/2021",
    salt: "FVOUIk",
    phone: "847-313-1289",
  },
  {
    id: 3,
    card_number: "5610480363247475108",
    card_type: "china-unionpay",
    issue_date: "5/7/2021",
    salt: "jBQThr",
    phone: "348-326-7873",
  },
  {
    id: 4,
    card_number: "374283660946674",
    card_type: "americanexpress",
    issue_date: "1/13/2021",
    salt: "n25JXsxzYr",
    phone: "599-331-8099",
  },
  {
    id: 5,
    card_number: "67090853951061268",
    card_type: "laser",
    issue_date: "3/18/2021",
    salt: "Yy5rjSJw",
    phone: "850-191-9906",
  },
  {
    id: 6,
    card_number: "560221984712769463",
    card_type: "china-unionpay",
    issue_date: "6/29/2021",
    salt: "VyyrJbUhV60",
    phone: "683-417-5044",
  },
  {
    id: 7,
    card_number: "3589433562357794",
    card_type: "jcb",
    issue_date: "11/16/2021",
    salt: "9M3zon",
    phone: "634-798-7829",
  },
  {
    id: 8,
    card_number: "5602255897698404",
    card_type: "china-unionpay",
    issue_date: "1/1/2021",
    salt: "YIMQMW",
    phone: "228-796-2347",
  },
  {
    id: 9,
    card_number: "3534352248361143",
    card_type: "jcb",
    issue_date: "4/28/2021",
    salt: "zj8NhPuUe4I",
    phone: "228-796-2347",
  },
  {
    id: 10,
    card_number: "4026933464803521",
    card_type: "visa-electron",
    issue_date: "10/1/2021",
    salt: "cAsGiHMFTPU",
    phone: "372-887-5974",
  },
];

/* 

    1. Find all card numbers whose sum of all the even position digits is odd.
    2. Find all cards that were issued before June.
    3. Assign a new field to each card for CVV where the CVV is a random 3 digit number.
    4. Add a new field to each card to indicate if the card is valid or not.
    5. Invalidate all cards issued before March.
    6. Sort the data into ascending order of issue date.
    7. Group the data in such a way that we can identify which cards were assigned in which months.

    Use method chaining to solve #3, #4, #5, #6 and #7.

    NOTE: Do not change the name of this file 
*/

//    1. Find all card numbers whose sum of all the even position digits is odd.
function get_card_number(cards_array) {
  let card_number_even_position = cards_array.filter((each) => {
    let card_number_array = [...each.card_number];
    let sum_value = card_number_array.reduce((acc, value, index) => {
      if (index % 2 === 0) {
        acc += Number(value);
        return acc;
      }
      return acc;
    }, 0);
    if (sum_value % 2 === 1) {
      return each;
    }
  });
  return card_number_even_position;
}

const get_card_number_result = get_card_number(cards_array);
console.log(get_card_number_result);

//2.Find all cards that were issued before June.
function cards_issued_before_june(cards_array) {
  let cards_data = cards_array.filter((each) => {
    if (Number(each.issue_date.split("/")[0]) < 6) {
      return each;
    }
  });
  return cards_data;
}

const cards_issued_before_june_result = cards_issued_before_june(cards_array);
console.log(cards_issued_before_june_result);

// Using method chaining to solve #3, #4, #5, #6 and #7..

function adding_cvv(cards_array) {
  let adding_cvv_new_field = cards_array.map((each) => {
    let random_cvv = Math.floor(Math.random() * (999 - 100 + 1)) + 100;
    each.cvv = random_cvv;
    return each;
  });
  const valid_field = cards_array.map((each) => {
    each.valid = true;
    return each;
  });
  const before_march_cards = cards_array.map((each) => {
    if (Number(each.issue_date.split("/")[0]) < 3) {
      each.valid = false;
    }
    return each;
  });
  let ascending_by_date_array = cards_array.sort((date_1, date_2) => {
    let formet_date_1 = new Date(date_1.issue_date);
    let formet_date_2 = new Date(date_2.issue_date);
    if (formet_date_1 > formet_date_2) {
      return 1;
    } else if (formet_date_1 < formet_date_2) {
      return -1;
    } else {
      return 0;
    }
  });
  const group_by_month_array = cards_array.reduce((output, each) => {
    let date_array = each.issue_date.split("/");
    let month = date_array[0];
    output[month] ?? (output[month] = []);
    if (each.issue_date.includes(month)) {
      output[month].push(each);
    }

    return output;
  }, {});
  return group_by_month_array;
}
const adding_fields = adding_cvv(cards_array);
console.log(adding_fields);
