let inputArray = [
  {
    id: 1,
    first_name: "Valera",
    last_name: "Pinsent",
    email: "vpinsent0@google.co.jp",
    gender: "Male",
    ip_address: "253.171.63.171",
  },
  {
    id: 2,
    first_name: "Kenneth",
    last_name: "Hinemoor",
    email: "khinemoor1@yellowbook.com",
    gender: "Polygender",
    ip_address: "50.231.58.150",
  },
  {
    id: 3,
    first_name: "Roman",
    last_name: "Sedcole",
    email: "rsedcole2@addtoany.com",
    gender: "Genderqueer",
    ip_address: "236.52.184.83",
  },
  {
    id: 4,
    first_name: "Lind",
    last_name: "Ladyman",
    email: "lladyman3@wordpress.org",
    gender: "Male",
    ip_address: "118.12.213.144",
  },
  {
    id: 5,
    first_name: "Jocelyne",
    last_name: "Casse",
    email: "jcasse4@ehow.com",
    gender: "Agender",
    ip_address: "176.202.254.113",
  },
  {
    id: 6,
    first_name: "Stafford",
    last_name: "Dandy",
    email: "sdandy5@exblog.jp",
    gender: "Female",
    ip_address: "111.139.161.143",
  },
  {
    id: 7,
    first_name: "Jeramey",
    last_name: "Sweetsur",
    email: "jsweetsur6@youtube.com",
    gender: "Genderqueer",
    ip_address: "196.247.246.106",
  },
  {
    id: 8,
    first_name: "Anna-diane",
    last_name: "Wingar",
    email: "awingar7@auda.org.au",
    gender: "Agender",
    ip_address: "148.229.65.98",
  },
  {
    id: 9,
    first_name: "Cherianne",
    last_name: "Rantoul",
    email: "crantoul8@craigslist.org",
    gender: "Genderfluid",
    ip_address: "141.40.134.234",
  },
  {
    id: 10,
    first_name: "Nico",
    last_name: "Dunstall",
    email: "ndunstall9@technorati.com",
    gender: "Female",
    ip_address: "37.12.213.144",
  },
];

/* 

    1. Find all people who are Agender
    2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
    3. Find the sum of all the second components of the ip addresses.
    3. Find the sum of all the fourth components of the ip addresses.
    4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
    5. Filter out all the .org emails
    6. Calculate how many .org, .au, .com emails are there
    7. Sort the data in descending order of first name

    NOTE: Do not change the name of this file

*/

//1. Find all people who are Agender

function agender(inputArray) {
  const outputArraywithAgender = inputArray.reduce((output, each) => {
    if (each.gender === "Agender") {
      output.push(each);
    }
    return output;
  }, []);
  return outputArraywithAgender;
}
const outputArraywithAgender = agender(inputArray);
console.log(outputArraywithAgender);

// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]

function ipArray(inputArray) {
  const splittedArray = inputArray.map((each) => {
    each.ip_address = each.ip_address.split(".");
    return each;
  });
  return splittedArray;
}
const splittedArray = ipArray(inputArray);
console.log(splittedArray);

// 3. Find the sum of all the second components of the ip addresses.

function sumOfIpaddressPositionTwo(inputArray, index) {
  const sumResult = inputArray.reduce((sum, each) => {
    sum += Number(each.ip_address[index]);
    return sum;
  }, 0);
  return sumResult;
}

const sumOfIpAddress = sumOfIpaddressPositionTwo(inputArray, 1);
console.log(sumOfIpAddress);
const sumOfIpAddressAtFourthPosition = sumOfIpaddressPositionTwo(inputArray, 3);
console.log(sumOfIpAddressAtFourthPosition);

// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.

function fullName(inputArray) {
  const newArrayWithFullname = inputArray.map((each) => {
    each.full_name = `${each.first_name} ${each.last_name}`;
    return each;
  });
  return newArrayWithFullname;
}
const newArrayWithFullname = fullName(inputArray);
console.log(newArrayWithFullname);

// 5. Filter out all the .org emails

function filterOrgMails(inputArray) {
  const resultArray = inputArray.filter((each) => each.email.endsWith(".org"));
  return resultArray;
}
const filterdArray = filterOrgMails(inputArray);
console.log(filterdArray);
// 6. Calculate how many .org, .au, .com emails are there
function countOfMails(inputArray) {
  let orgMailCount = 0;
  let auMailCount = 0;
  let comMailCount = 0;
  inputArray.forEach((each) => {
    if (each.email.endsWith(".org")) {
      orgMailCount += 1;
    } else if (each.email.endsWith(".au")) {
      auMailCount += 1;
    } else if (each.email.endsWith(".com")) {
      comMailCount += 1;
    }
  });
  return [orgMailCount, auMailCount, comMailCount];
}
const mailCount = countOfMails(inputArray);
console.log(mailCount);

//7. Sort the data in descending order of first name

function sortByFirstname(inputArray) {
  const resultArray = inputArray.sort((name1, name2) => {
    const firstName1 = name1.first_name.toLowerCase();
    const firstName2 = name2.first_name.toLowerCase();
    if (firstName1 > firstName2) {
      return -1;
    }
    if (firstName1 < firstName2) {
      return 1;
    }

    return 0;
  });
  return resultArray;
}
const sortedByFirstnameResult = sortByFirstname(inputArray);
console.log(sortedByFirstnameResult);
